---
title: Squad 5
author: Leandro Lara, João Pedro Duarte e Tales Moreira
---



# Referências 

* [Site ChefTime](https://cheftime.com.br/)
* IFood
* UberEats
* Rappi

# Desk research

## Evidências

* O modelo de envio de ingredientes é consolidado na américa do norte e na europa
* Os canais de culinária no youtube cresceram significativamente no último ano
* O modelo de entrega por aplicativo é consolidado e confiado no Brasil

## Insights

* Como é um marketplace, o público alvo é mais diverso que do cheftime
* Permitir sugestão de receitas pelos usuários


# CSD

## Certezas
* Já tem aqui no Brasil
* Mais pessoas em casa durante a pandemia
* Mais pessoas curiosas com culinária durante a pandemia 

## Suposições
* As pessoas que gostam de cozinhar vão aderir ao aplicativo
* Pessoas curiosas vão querer aderir


## Dúvidas
* Será que vão aderir?
* Precisaremos de duas versões do aplicativo?
* Permitir que o usuário compre produtos de mercados diferentes de um mesmo pacote?

***

# Persona

* Mora na cidade grande

## Motivação

* Gosta de cozinhar
* Pouco desperdício
* Tem interesse  em cozinhar


## Frustração

* Medo da qualidade do produto
* Qualidade da comida pronta
* Está entediado durante a quarentena
