const { Router } = require('express');
const UserController = require('../controllers/UserController');
const DishController = require('../controllers/DishController');
const IngredientsController = require('../controllers/IngredientsController');
const OrderController = require('../controllers/OrderController');
const RecipeController = require('../controllers/RecipeController');
const RoleController = require('../controllers/RoleController');
const ReviewController = require('../controllers/ReviewController');
const AuthController = require("../controllers/AuthController");
const PhotoController = require("../controllers/PhotoController");
const router = Router();
const passport = require("passport");
const adminMiddleware = require("../middlewares/admin")
const vendorMiddleware = require("../middlewares/vendor")
const sameUserMiddleware = require("../middlewares/sameUser")
const path = require('path');
const multer = require('multer');
const storage = require("../config/files");
const validator = require('../config/validator')


router.use("/private", passport.authenticate('jwt', {session: false}));
router.get('/private/getDetails', AuthController.getDetails);
router.post('/login', AuthController.login);
router.post('/logout', AuthController.logout);

router.get('/users', UserController.index);
router.get('/users/:id', UserController.show);
router.delete('/users/:id', adminMiddleware, UserController.destroy);
router.post('/users', validator.validationUser('create'), UserController.create);
router.put('/users/:id',validator.validationUser('update'),UserController.update);
router.put('/like/:id', UserController.like);
router.put('/dislike/:id', UserController.dislike);
router.get('/users/likes/:id', UserController.likeList);


router.get('/dish',DishController.index);
router.get('/dish/:id',DishController.show);
router.post('/dish',DishController.create);
router.put('/dish/:id', adminMiddleware, DishController.update);
router.delete('/dish/:id', adminMiddleware, DishController.destroy);

router.get('/ingredients',IngredientsController.index);
router.get('/ingredients/:id',IngredientsController.show);
router.post('/ingredients',validator.validationIngredient('create'), vendorMiddleware,IngredientsController.create);
router.put('/ingredients/:id',validator.validationIngredient('create'), vendorMiddleware, sameUserMiddleware, IngredientsController.update);
router.delete('/ingredients/:id', vendorMiddleware, IngredientsController.destroy);
router.get('/recipe/ingredients/:id', IngredientsController.search)

router.get('/order',OrderController.index);
router.get('/order/:id',OrderController.show);
router.post('/order',validator.validationOrder('create'),OrderController.create);
router.put('/order/:id',validator.validationOrder('create'), OrderController.update);
router.delete('/order/:id', OrderController.destroy);
router.put('/addItem/:id', OrderController.addItem);
router.put('/removeItem/:id', OrderController.removeItem);


router.get('/recipe',RecipeController.index);
router.get('/recipe/:id',RecipeController.show);
router.post('/recipe',validator.validationRecipe('create'),vendorMiddleware, RecipeController.create);
router.put('/recipe/:id',validator.validationRecipe('create'), vendorMiddleware, RecipeController.update);
router.delete('/recipe/:id', vendorMiddleware, RecipeController.destroy);
router.post('/recipe/photo/:id', vendorMiddleware, PhotoController.upload.single('photo'), RecipeController.addRecipePhoto);
router.delete('/recipe/photo/:id', vendorMiddleware, RecipeController.removeRecipePhoto);
router.get('/search', RecipeController.search);
 

router.get('/role',RoleController.index);
router.get('/role/:id',RoleController.show);
router.post('/role',RoleController.create);
router.put('/role/:id', RoleController.update);
router.delete('/role/:id', RoleController.destroy);

router.get('/review',ReviewController.index);
router.get('/review/:id',ReviewController.show);


router.delete('/review/:id', sameUserMiddleware, ReviewController.destroy);
router.post('/review', validator.validationReview('create'), ReviewController.create);
router.put('/review/:id',validator.validationReview('create'), sameUserMiddleware, ReviewController.update);


router.post('/users/photo/:id', PhotoController.upload.single('photo'), UserController.addUserPhoto);
router.delete('/photo/:id', UserController.removeUserPhoto);
 

module.exports = router;