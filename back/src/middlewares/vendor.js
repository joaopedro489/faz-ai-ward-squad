const Role = require('../models/RoleModel');

const vendor = async(req, res, next) => {
    try{
        const role = await Role.findOne({where : {UserId: req.body.UserId}});        
        if (role.name == 'admin' || role.name == 'vendor') return next();
        else return res.status(401).json({'error' : 'Sem autorização'});
    } catch (e){
        return res.status(500).json(e + "!");
    };
}

module.exports = vendor;