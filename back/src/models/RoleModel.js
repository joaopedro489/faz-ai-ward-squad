const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Role = sequelize.define('Role', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    }

});

Role.associate = function(models) {
    Role.belongsTo(models.User);
}

module.exports = Role;