const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");
const Photo = require("./PhotoModel");

const User = sequelize.define('User', {
    email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
    },

    name: {
        type: DataTypes.STRING,
        allowNull: false

    },

    phone: {
        type: DataTypes.STRING,
        allowNull: false
    },

    address: {
        type: DataTypes.STRING,
        allowNull: false
    },
    cnpj: {
        type: DataTypes.STRING,
        allowNull: true
    },

    hash: {
		type: DataTypes.STRING
	},
    
	salt: {
		type: DataTypes.STRING
	}
});

User.associate = function(models) {
    User.hasMany(models.Order);
    User.hasMany(models.Review);
    User.hasOne(models.Role);
    User.hasMany(models.Recipe);
    User.belongsToMany(models.Recipe, { through: 'Like', as: 'liking'});
    User.hasOne(models.Photo);
}

module.exports = User;