const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");


const Order = sequelize.define('Order', {
    date: {
        type: DataTypes.DATE,
        allowNull: true
    },

    status: {
        type: DataTypes.STRING,
        allowNull: false
    }

});

Order.associate = function(models) {
    Order.belongsTo(models.User);
    Order.belongsToMany(models.Ingredients , {through: models.Cart, as: 'order', foreignKey: 'IngredientId'} );
}

module.exports = Order;