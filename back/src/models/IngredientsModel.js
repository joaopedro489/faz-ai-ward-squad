const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Ingredients = sequelize.define('Ingredients', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    price: {
        type: DataTypes.NUMBER,
        allowNull: false
    },

    // quantity: {
    //     type: DataTypes.NUMBER,
    //     allowNull: false
    // },

    portion: {
        type: DataTypes.STRING,
        allowNull: false
    }
});

Ingredients.associate = function(models) {
    Ingredients.belongsTo(models.Recipe);
    Ingredients.belongsToMany(models.Order, {through: models.Cart , as: 'inCart', foreignKey: 'OrderId' });
}

module.exports = Ingredients;