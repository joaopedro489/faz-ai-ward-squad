const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Recipe = sequelize.define('Recipe', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    difficulty: {
        type: DataTypes.NUMBER,
        allowNull: false
    },
    content:{
        type:DataTypes.STRING
    }

});

Recipe.associate = function(models) {
    Recipe.hasOne(models.Dish);
    Recipe.hasMany(models.Ingredients);
    Recipe.belongsTo(models.User);
    Recipe.belongsToMany(models.User, { through: 'Like', as: 'liked' });
    Recipe.hasOne(models.RecipePhoto);
}


module.exports = Recipe;