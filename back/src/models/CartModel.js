const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Cart = sequelize.define('Cart', {
    quantity: {
        type: DataTypes.NUMBER,
        allowNull: true
    }
});


module.exports = Cart;