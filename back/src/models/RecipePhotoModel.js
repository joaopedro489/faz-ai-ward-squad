const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const RecipePhoto = sequelize.define("RecipePhoto", {
		path: {
			type: DataTypes.STRING,
			allowNull: false
		}
});

RecipePhoto.associate = function(models){
	RecipePhoto.belongsTo(models.Recipe);
}
module.exports = RecipePhoto;