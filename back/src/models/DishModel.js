const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Dish = sequelize.define('Dish', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    }
});

Dish.associate = function(models) {
    Dish.belongsToMany(models.Recipe, {through: 'Categories'});
}

module.exports = Dish;