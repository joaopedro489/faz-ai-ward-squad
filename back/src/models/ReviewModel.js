const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Review = sequelize.define('Review', {
    title: {
        type: DataTypes.STRING,
        allowNull: false
    },
    content: {
        type: DataTypes.STRING,
        allowNull: false
    },
    rate: {
        type: DataTypes.NUMBER,
        allowNull: false
    }

});

Review.associate = function(models) {
    Review.belongsTo(models.User);
    Review.belongsTo(models.Recipe);

}

module.exports = Review;