const { response } = require('express');
const Dish = require('../models/DishModel');

const create = async(req,res) => {
    try{
          const dish = await Dish.create(req.body);
          return res.status(201).json({message: "Prato cadastrado com sucesso!", dish: dish});
      }catch(err){
          res.status(500).json({error: err});
      }
};

const index = async(req,res) => {
    try {
        const dishes = await Dish.findAll();
        return res.status(200).json({dishes});
    }catch(err){
        return res.status(500).json({err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const dish = await Dish.findByPk(id);
        return res.status(200).json({dish});
    }catch(err){
        return res.status(500).json({err});
    }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Dish.update(req.body, {where: {id: id}});
        if(updated) {
            const dish = await Dish.findByPk(id);
            return res.status(200).send(dish);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Prato não encontrado");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Dish.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Prato deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Prato não encontrado.");
    }
};

module.exports = {
    index,
    show,
    create,
    update,
    destroy
};

