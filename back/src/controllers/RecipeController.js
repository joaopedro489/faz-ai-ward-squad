const { response } = require('express');
const Recipe = require('../models/RecipeModel');
const User = require('../models/UserModel');
const {validationResult} = require('express-validator');
const Photo = require('../models/RecipePhotoModel');
const fsPromise = require('fs').promises;
const path = require('path');
const {Op} = require('sequelize');


const create = async(req,res) => {
    try{
        validationResult(req).throw();
          const recipe = await Recipe.create(req.body);
          return res.status(201).json({recipe});
      }catch(err){
          res.status(500).json({error: err});
      }
};

const index = async(req,res) => {
    try {
        const recipes = await Recipe.findAll();
        return res.status(200).json({recipes});
    }catch(err){
        return res.status(500).json({err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const recipe = await Recipe.findByPk(id);
        return res.status(200).json({recipe});
    }catch(err){
        return res.status(500).json({err});
    }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        validationResult(req).throw();
        const [updated] = await Recipe.update(req.body, {where: {id: id}});
        if(updated) {
            const recipe = await Recipe.findByPk(id);
            return res.status(200).send(recipe);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json({err});
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Recipe.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Receita deletada com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Receita não encontrada.");
    }
};

const addRecipePhoto = async(req, res) => {
    try {
        const {id} = req.params;
        const recipe = await Recipe.findByPk(id, {include:{model: Photo}});
        if(req.file){
            const path = process.env.APP_URL + "/uploads/" + req.file.filename;
            console.log("path");
            const photo = await Photo.create({
                path: path,
                RecipeId: id
            });
        }
        await recipe.reload();
        return res.status(200).json(recipe);
    } catch (e) {
        return res.status(500).json(e + "!");
    }
}

const removeRecipePhoto = async(req, res) => {
    try {
        const {id} = req.params;
        const photo  = await Photo.findOne({ where: { RecipeId: id } });
        const pathDb = photo.path.split("/").slice(-1)[0];
        await fsPromise.unlink(path.join(__dirname, "..", "..", "uploads", pathDb));
        await photo.destroy();
        return res.status(200).json("Foto deletada com sucesso");
    } catch (e) {
        return res.status(500).json(e + "!");
    }
}

const search = async(req,res) => {
    
    try {
        const {search} = req.body;
        const recipes = await Recipe.findAll({
            where:{
                [Op.or]:{
                    name: {
                        [Op.like]: '%' + search + '%'
                    },
                    content: {
                        [Op.like]: '%' + search + '%'
                    }
                }
            }
        });
        return res.status(200).json({recipes});
    } catch(e){
        return res.status(500).json(e + "!");
    
    }
}

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    addRecipePhoto,
    removeRecipePhoto,
    search
};

