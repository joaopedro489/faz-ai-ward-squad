const { response } = require('express');
const User = require('../models/UserModel');
const Recipe = require('../models/RecipeModel')
const Photo = require('../models/PhotoModel');
const Auth = require("../config/auth");
const fsPromise = require('fs').promises;
const path = require('path');
const mailer = require('../config/mail').mailer;
const readHtml = require("../config/mail").readHTMLFile;
const hbs = require("handlebars");
const Role = require('../models/RoleModel');
const {validationResult} = require('express-validator');


const create = async(req,res) => {
	try {
        /* Validação */
        validationResult(req).throw(); 
        
        /* Criação do usuário */
		const { password } = req.body;
		const hashAndSalt = Auth.generatePassword(password);
		const salt = hashAndSalt.salt;
		const hash = hashAndSalt.hash;
		const newUserData = {
			email: req.body.email,
			name: req.body.name,
			phone: req.body.phone,
			address: req.body.address,
            cnpj: req.body.cnpj,
			hash: hash,
			salt: salt
		}

        const user = await User.create(newUserData);

        /* Atribuindo cargo caso receba cnpj */
        var name = 'user'
        if(req.body.cnpj != null || req.body.cnpj != undefined ){
            name = 'vendor'
        }
        const RoleData = {
            name: name,
            UserId: user.id
        }
        const role = await Role.create(RoleData);

        /* Nodemailer */
        const pathTemplate = path.resolve(__dirname, '..', '..', 'templates');
		console.log(pathTemplate);
        readHtml(path.join(pathTemplate, "main.html"), (err,html)=>{
            const template = hbs.compile(html);
            const replacements = {
                username: user.name
            };
            const htmlToSend = template(replacements);
            const message = {
                from: "fazaiwardsquad@gmail.com",
                to: user.email,
                subject: "Apenas um teste",
                html: htmlToSend
            }
            mailer.sendMail(message, (e) => {
                console.log(e + "!");
            });
        });

		return res.status(201).json({user,role});
	} catch (err) {
		return res.status(500).json({err});
	}
}

const index = async(req,res) => {
    try {
        const users = await User.findAll();
        return res.status(200).json({users});
    }catch(err){
        return res.status(500).json({err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        return res.status(200).json({user});
    }catch(err){
        return res.status(500).json({err});
    }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        validationResult(req).throw(); /* Validador */

        /* Geração de novo hash/salt com a nova senha */
        const password  = req.body.password;
		const hashAndSalt = Auth.generatePassword(password); 
		const salt = hashAndSalt.salt; 
		const hash = hashAndSalt.hash;
		const UserData = { 
			email: req.body.email,
			name: req.body.name,
			phone: req.body.phone,
			address: req.body.address,
            cnpj: req.body.cnpj,
			hash: hash,
			salt: salt
		}

        const [updated] = await User.update(UserData, {where: {id: id}});
        
        if(updated) {
            const user = await User.findByPk(id);
            return res.status(200).send(user);
        } 
        throw new Error();
    }catch(e){
        return res.status(500).json(e + '!');
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await User.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Usuário deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Usuário não encontrado.");
    }
};

const like = async(req,res) => {
    const {id} = req.params;
    try {
        const userLiking = await User.findByPk(id);
        const recipeLiked = await Recipe.findByPk(req.body.recipeId);
        await userLiking.addLiking(recipeLiked);
        return res.status(200).json({userLiking,recipeLiked});
        }
        catch(err){
        return res.status(500).json({err});
        }
    }

    const dislike = async(req,res) => {
        const {id} = req.params;
        try {
            const userLiking = await User.findByPk(id);
            const recipeLiked = await Recipe.findByPk(req.body.recipeId);
            await userLiking.removeLiking(recipeLiked);
            return res.status(200).json(userLiking);
            }
            catch(err){
            return res.status(500).json({err});
            }
        }

    const addUserPhoto = async(req, res) => {
        try {
            const {id} = req.params;
            const user = await User.findByPk(id, {include:{model: Photo}});
            if(req.file){
                const path = process.env.APP_URL + "/uploads/" + req.file.filename;
                console.log("path");
    
                const photo = await Photo.create({
                    path: path,
                    UserId: id
                });
            }
            await user.reload();
            return res.status(200).json(user);
        } catch (e) {
            return res.status(500).json(e + "!");
        }
    }



    const likeList = async(req,res) => {
    const {id} = req.params;
    try {
        const user = await User.findByPk(id);
        const list = await user.getLiking();
        return res.status(200).json(list);
        }
        catch(e){
        return res.status(500).json(e + "!");
        }
    }


    const removeUserPhoto = async(req, res) => {
        try {
            const {id} = req.params;
            const photo  = await Photo.findOne({ where: { UserId: id } });
            const pathDb = photo.path.split("/").slice(-1)[0];
            await fsPromise.unlink(path.join(__dirname, "..", "..", "uploads", pathDb));
            await photo.destroy();
            return res.status(200).json("Foto deletada com sucesso");
        } catch (e) {
            return res.status(500).json(e + "!");
        }
    }

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    like,
    dislike,
    addUserPhoto,
    removeUserPhoto,
    likeList
};

