const { response } = require('express');
const Ingredient = require('../models/IngredientsModel');
const Recipe = require('../models/RecipeModel');
const {validationResult} = require('express-validator');


const create = async(req,res) => {
    try{
        validationResult(req).throw();
          const ingredient = await Ingredient.create(req.body);
          return res.status(201).json({message: "Ingrediente cadastrado com sucesso!", ingredient: ingredient});
      }catch(err){
          res.status(500).json({error: err});
      }
};

const index = async(req,res) => {
    try {
        const ingredients = await Ingredient.findAll();
        return res.status(200).json({ingredients});
    }catch(err){
        return res.status(500).json({err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const ingredient = await Ingredient.findByPk(id);
        return res.status(200).json({ingredient});
    }catch(err){
        return res.status(500).json({err});
    }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        validationResult(req).throw();
        const [updated] = await Ingredient.update(req.body, {where: {id: id}});
        if(updated) {
            const ingredient = await Ingredient.findByPk(id);
            return res.status(200).send(ingredient);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json({err});
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Ingredient.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Ingrediente deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Ingrediente não encontrado.");
    }
};

const search = async(req,res) => {
    
    try {
        const {id} = req.params;
        const recipe = await Recipe.findByPk(id)
        const ingredients = await Ingredient.findAll({
            where:{
                RecipeId: id
            }
        });
        return res.status(200).json({recipe,ingredients});
    } catch(e){
        return res.status(500).json(e + "!");
    
    }
}

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    search
};

