const { response } = require('express');
const Role = require('../models/RoleModel');

const create = async(req,res) => {
    try{
          const role = await Role.create(req.body);
          return res.status(201).json({message: "Cargo cadastrado com sucesso!", role: role});
      }catch(err){
          res.status(500).json({error: err});
      }
};

const index = async(req,res) => {
    try {
        const roles = await Role.findAll();
        return res.status(200).json({roles});
    }catch(err){
        return res.status(500).json({err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const role = await Role.findByPk(id);
        return res.status(200).json({role});
    }catch(err){
        return res.status(500).json({err});
    }
};

const update = async(req,res) => {
    try {
        const updated = await Role.findByPk(req.body.id);
        if(updated != null) {
            updated.name = req.body.name;
            const role = await updated.save();
            return res.status(200).send(role);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Cargo não encontrado");
    }
};

const destroy = async(req,res) => {
    try {
        const deleted = await Role.destroy({where: {id: req.body.id}});
        if(deleted) {
            return res.status(200).json("Cargo deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Cargo não encontrado.");
    }
};

module.exports = {
    index,
    show,
    create,
    update,
    destroy
};

