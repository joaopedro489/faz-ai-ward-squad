const { response } = require('express');
const Ingredients = require('../models/IngredientsModel');
const Order = require('../models/OrderModel');
const {validationResult} = require('express-validator');


const create = async(req,res) => {
    try{
        validationResult(req).throw();
        const order = await Order.create(req.body);
        return res.status(201).json({message: "Pedido cadastrado com sucesso!", order: order});
      }catch(err){
          res.status(500).json({err});
      }
};

const index = async(req,res) => {
    try {
        const orders = await Order.findAll();
        return res.status(200).json({orders});
    }catch(err){
        return res.status(500).json({err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const order = await Order.findByPk(id);
        return res.status(200).json({order});
    }catch(err){
        return res.status(500).json({err});
    }
};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        validationResult(req).throw();
        const [updated] = await Order.update(req.body, {where: {id: id}});
        if(updated) {
            const order = await Order.findByPk(id);
            return res.status(200).send(order);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json({err});
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Order.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Pedido deletado com sucesso.");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Pedido não encontrado.");
    }
};

const addItem = async(req,res) => {
    const {id} = req.params;
    try {
        const quantity = req.body.quantity
        const order = await Order.findByPk(id);
        const ingredient = await Ingredients.findByPk(req.body.ingredientsId);
        await order.addOrder(ingredient,{ through: { quantity: quantity }})
        return res.status(200).json(order,);
        }
        catch(e){
        return res.status(500).json(e + '!');
        }
    }

    const removeItem = async(req,res) => {
        const {id} = req.params;
        try {
            const order = await Order.findByPk(id);
            const ingredient = await Ingredients.findByPk(req.body.ingredientsId);
            await order.removeOrder(ingredient);
            return res.status(200).json(order);
            }
            catch(err){
            return res.status(500).json({err});
            }
        }

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    addItem,
    removeItem
};

