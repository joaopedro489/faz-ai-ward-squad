const { response } = require('express');
const Review = require('../models/ReviewModel');
const {validationResult} = require('express-validator');

const create = async(req,res) => {
    try{    
        validationResult(req).throw();
		const review = await Review.create(req.body);
        return res.status(201).json({review});
      }catch(err){
        res.status(500).json({error: err});
      }
};

const index = async(req,res) => {
    try {
        const reviews = await Review.findAll();
        return res.status(200).json({reviews});
    }catch(err){
        return res.status(500).json({err});
    }
};

const show = async(req,res) => {
    const {id} = req.params;
    try {
        const review = await Review.findByPk(id);
        return res.status(200).json({review});
    }catch(err){
        return res.status(500).json({err});
    }
};

const update = async(req,res) => {
    try {
        validationResult(req).throw();
        const ReviewData = { 
            title: req.body.title,
            content: req.body.content,
            rate: req.body.rate,
            RecipeId: req.body.RecipeId,
            UserId: req.body.UserId,
            ReviewId: req.body.ReviewId
        }
        const updated = await Review.findByPk(ReviewData.ReviewId);
        if(updated != null) {
            updated.title = req.body.title;
            updated.content = req.body.content;
            updated.rate = req.body.rate;
            updated.RecipeId = req.body.RecipeId;
            updated.UserId = req.body.UserId;
            const review = await updated.save();
            return res.status(200).send(review);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json({err});
    }
};

const destroy = async(req,res) => { 
    try {
        const deleted = await Review.destroy({where: {id: req.body.id}});
        if(deleted) {
            return res.status(200).json("Avaliação deletada com sucesso.");
        }
        throw new Error ();
    }catch(e){
        return res.status(500).json(e + '!');
    }
};

module.exports = {
    index,
    show,
    create,
    update,
    destroy
};

