const { body } = require("express-validator");



const validationUser = (method) =>{
    switch(method){
        case 'create': {
            return [
                body('email').exists().withMessage("This Field mustn't be null").isLength({min: 7}).withMessage('Por favor, preencha o campo com um email').isEmail().withMessage('Precisa ser exemplo@exemplo'),
                body('name').exists().withMessage("This Field mustn't be null").isLength({min: 2}).withMessage('Por favor, preencha com um nome válido'),
                body('phone').exists().withMessage("This Field mustn't be null").isLength({min: 10}).withMessage('Por favor, preencha com um número válido como (DDD) 91234-5678'),
                body('address').exists().withMessage("This Field mustn't be null"),
                body('password').exists().withMessage("This Field mustn't be null").isLength({min: 5}).withMessage('Sua senha precisa ter mais de 4 dígitos')

            ]
        }
        case 'update': {
            return [
                body('email').isLength({min: 7}).withMessage('Por favor, preencha o campo com um email').isEmail().withMessage('Precisa ser exemplo@exemplo'),
                body('name').isLength({min: 2}).withMessage('Por favor, preencha com um nome válido'),
                body('phone').isLength({min: 10}).withMessage('Por favor, preencha com um número válido como (DDD) 91234-5678'),                
                body('phone').isLength({min: 1}).withMessage('Por favor, preencha com um número válido'),
                body('password').isLength({min: 5}).withMessage('Sua senha precisa ter mais de 4 dígitos')
            ]
        }
    }
}
const validationReview = (method) =>{
    switch(method){
        case 'create': {
            return [
            body('title').exists().withMessage("This Field mustn't be null").isLength({min: 2}).withMessage('Preencha o campo com um título'),
            body('content').exists().withMessage("This Field mustn't be null").isLength({min: 3}).withMessage('Preencha o campo com seu comentário'),
            body('rate').exists().withMessage("This Field mustn't be null").isLength({min: 1 ,max: 1}).isInt({max: 5}),
            body('RecipeId').exists().withMessage("Sem id da receita"),
            body('UserId').exists().withMessage("Sem id do usuário")
            ]
        }
    }
}

const validationIngredient = (method) =>{
    switch(method){
        case 'create': {
            return [
                body('name').exists().withMessage("This Field mustn't be null").isLength({min: 2}).withMessage('Preencha o campo com um nome'),
                body('price').exists().withMessage("This Field mustn't be null").isNumeric().withMessage('Preencha o campo com um valor numérico'),
                body('quantity').exists().withMessage("This Field mustn't be null").isNumeric().withMessage('Preencha o campo com um valor numérico'),
                body('portion').exists().withMessage('Preencha o campo com o tamanho da porção').isLength({min: 2}),
                body('RecipeId').exists().withMessage("This Field mustn't be null")
            ]
        }
    }
}

const validationRecipe = (method) =>{
    switch(method){
        case 'create': {
            return [
                body('name').exists().withMessage("This Field mustn't be null").isLength({min: 2}).withMessage('Preencha o campo com um nome'),
                body('difficulty').exists().withMessage("This Field mustn't be null").isNumeric().withMessage('Preencha o campo com um valor numérico'),
                body('UserId').exists().withMessage("This Field mustn't be null").isNumeric().withMessage('Preencha o campo com um valor numérico')
            ]
        }
    }
}

const validationOrder = (method) =>{
    switch(method){
        case 'create': {
            return [
                body('date').exists().withMessage("This Field mustn't be null").isDate().withMessage('Preencha o campo com uma data'),
                body('status').exists().withMessage("This Field mustn't be null").isLength({min: 3}),
                body('UserId').exists().withMessage("This Field mustn't be null").isNumeric().withMessage('Preencha o campo com um valor numérico')
            ]
        }
    }
}


module.exports = {
    validationUser,
    validationReview,
    validationIngredient,
    validationRecipe,
    validationOrder
}
